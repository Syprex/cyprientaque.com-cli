use chrono::naive::NaiveDateTime;
use structopt::StructOpt;
use clap::arg_enum;
use std::time::Duration;
use serde;
use reqwest::{self, StatusCode, blocking};
use std::fs::{ self, File };
use std::env::{temp_dir, var};
use tokio::{self, task};
use std::process::Command;
use rand::Rng;
use std::io::prelude::*;
use rand::distributions::Alphanumeric;
use dialoguer::{ Confirm, Input, theme::ColorfulTheme, Select };
use jsonwebtoken::{encode, Header, EncodingKey};
use std::time::{SystemTime, UNIX_EPOCH};
use url::{Url, ParseError};
use mime_guess::MimeGuess;
use mime;

arg_enum!{
    #[derive(Debug)]
    #[allow(non_camel_case_types)]
    enum Entity {
        projects,
        categories,
        image_categories,
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "ctprods", about = "the stupid content tracker")]
enum Ctprods {
    #[structopt(name = "list")]
    List,
    #[structopt(name = "create")]
    Create,
    #[structopt(name = "add-image")]
    AddImage {
        #[structopt(short = "f", long = "file")]
        file: String,
    },
    #[structopt(name = "edit")]
    Edit,
    #[structopt(name="publish")]
    Publish,
    #[structopt(name="unpublish")]
    Unpublish
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize, std::cmp::PartialEq, std::cmp::PartialOrd, std::cmp::Eq, std::cmp::Ord)]
pub struct Project {
    pub id: i32,
    pub category_id: i32,
    pub title: String,
    pub slug: String,
    pub content: String,
    pub views_count: i32,
    pub likes_count: i32,
    pub deleted_at: Option<NaiveDateTime>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
    pub sketchfab_model_number: Option<String>,
    pub user_id: i32,
    pub published: bool,
    pub category: Option<ProjectCategory>,
    pub images: Option<Vec<ProjectImage>>,
    pub user: Option<User>
}

impl Project{
    fn pretty_print(&self){
        println!("id: {}, title: {}, published: {}", self.id, self.title, self.published);
    }
    fn edit (&mut self)-> UpdatableProject {
        let editor = var("EDITOR");
        let editor = match editor {
            Ok(editor) => editor,
            _ => "vim".to_string()
        };
        let file_name = gen_tmp_filename();
        let mut file = File::create(&file_name).unwrap();
        let mut w = Vec::new();
        write!(&mut w, "{}", &self.content).unwrap();
        file.write(&w).unwrap();
        Command::new(editor).arg(&file_name).status().expect("Cannot open file");
        let contents = fs::read_to_string(&file_name)
            .expect("Something went wrong reading the file");
        self.content = contents;
        self.to_updatable()
    }

    fn to_updatable(&self) -> UpdatableProject {
        UpdatableProject{
            id : self.id,
            title : self.title.clone(),
            content : self.content.clone(),
            category_id : self.category_id,
            user_id : self.user_id
        }
    }
    async fn publish(&self) -> Result<Project, reqwest::Error> {
        let base_url = String::from("projects");
        let base_url = format!("/{}/{}/publish", base_url, self.id.to_string());
        let url: Url = build_url(&base_url);
        let client = reqwest::Client::new();
        let project = client.put(url)
            .header("Authorization", request_auth())
            .send().await?
            .json::<Project>().await?;

        Ok(project)
    }
    async fn unpublish(&self) -> Result<Project, reqwest::Error> {
        let base_url = String::from("projects");
        let base_url = format!("/{}/{}/unpublish", base_url, self.id.to_string());
        let url: Url = build_url(&base_url);
        let client = reqwest::Client::new();
        let project = client.put(url)
            .header("Authorization", request_auth())
            .send().await?
            .json::<Project>().await?;

        Ok(project)
    }
}

#[derive(Clone, serde::Serialize, serde::Deserialize, Debug)]
pub struct NewProject {
    pub category_id: i32,
    pub title: String,
    pub slug: Option<String>,
    pub content: String,
    pub sketchfab_model_number: Option<String>,
    pub user_id: i32,
    pub is_pro: bool,
    pub bitbucket_project_key: Option<String>,
}

impl NewProject{
    fn edit (&mut self)-> &mut NewProject {
        let editor = var("EDITOR");
        let editor = match editor {
            Ok(editor) => editor,
            _ => "vim".to_string()
        };
        let file_name = gen_tmp_filename();
        let mut file = File::create(&file_name).unwrap();
        let mut w = Vec::new();
        write!(&mut w, "{}", &self.content).unwrap();
        file.write(&w).unwrap();
        Command::new(editor).arg(&file_name).status().expect("Cannot open file");
        let contents = fs::read_to_string(&file_name)
            .expect("Something went wrong reading the file");
        self.content = contents;
        self
    }
}

#[derive(serde::Deserialize,serde::Serialize, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct UpdatableProject {
    pub id : i32,
    pub title : String,
    pub content : String,
    pub category_id : i32,
    pub user_id : i32
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct Claims {
    exp: u64,          // Required (validate_exp defaults to true in validation). Expiration time (as UTC timestamp)
    iat: u64,          // Optional. Issued at (as UTC timestamp)
    user_id: i32,
}

fn request_auth () -> String {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards");
    let claims = Claims {
        exp: since_the_epoch.as_secs() + 120000,
        iat: since_the_epoch.as_secs(),
        user_id: 1
    };
    let token = encode(&Header::default(), &claims, &EncodingKey::from_secret("zfi6745lj43zeoij23zl6oei23jfzl4oejfl354z7efj".as_ref())).expect("Failed to encode JWT");
    let header = String::from("Bearer ");
    format!("{}{}", header, token)
}

impl UpdatableProject {
    async fn update(&self) -> Result<Project, String> {
        let base_url = String::from("/projects/");
        let base_url = format!("{}{}", base_url, &self.id.to_string());
        let url: Url = build_url(&base_url);
        let client = reqwest::Client::new();
        let resp = client.put(url)
            .header("Authorization", request_auth())
            .json(&self)
            .header("Content-Type", "application/json")
            .send()
            .await.unwrap();
        if resp.status() == StatusCode::BAD_REQUEST {
            let errors: String = resp.text().await.unwrap();
            Err(errors)
        }else if resp.status() == StatusCode::OK {
            let project: Project = resp.json::<Project>().await.unwrap();
            Ok(project)
        } else {
            let undefined_err: String = resp.text().await.unwrap();
            Err(undefined_err)
        }
    }
}

impl NewProject{
    async fn save(&self)-> Result<Project, String> {
        let url: Url = build_url("/projects");
        let client = reqwest::Client::new();
        let resp = client.post(url)
            .json(&self)
            .header("Content-Type", "application/json")
            .header("Authorization", request_auth())
            .send()
            .await.unwrap();

        if resp.status() == StatusCode::BAD_REQUEST {
            let errors = resp.json::<String>().await.unwrap();
            Err(errors)
        }else if resp.status() == StatusCode::OK {
            let project = resp.json::<Project>().await.unwrap();
            Ok(project)
        } else {
            Err(String::from ("Il y a eu un problème lors de l'enregistrement du projet"))
        }
    }
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize, std::cmp::PartialEq, std::cmp::PartialOrd, std::cmp::Eq, std::cmp::Ord)]
pub struct ProjectImage {
    id: i32,
    w1500_keyname: String,
    w350_keyname: String,
    w1500_object_url: String,
    original_object_url: Option<String>,
    w350_object_url: String,
    primary: bool,
    project_image_category_id: i32,
    project_id: i32,
    created_at: Option<NaiveDateTime>,
    updated_at: Option<NaiveDateTime>,
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize, std::cmp::PartialEq, std::cmp::PartialOrd, std::cmp::Eq, std::cmp::Ord)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub email: String,
    pub password: String,
    pub punchline: Option<String>,
    pub website_url: Option<String>,
    pub admin: bool,
    pub active: bool,
    pub deleted_at: Option<NaiveDateTime>,
    pub remember_token: Option<String>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
    pub api_token: Option<String>,
    pub profile_images: Option<Vec<ProfileUserImage>>
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize, std::cmp::PartialEq, std::cmp::PartialOrd, std::cmp::Eq, std::cmp::Ord)]
pub struct ProfileUserImage{
    pub id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: Option<NaiveDateTime>,
    pub deleted_at: Option<NaiveDateTime>,
    pub user_id: i32,
    pub w1500_keyname: String,
    pub w200_keyname: String,
    pub w40_keyname: String,
    pub w1500_object_url: String,
    pub w200_object_url: String,
    pub w40_object_url: String,
}


#[derive(Clone, Debug, serde::Serialize, serde::Deserialize, std::cmp::PartialEq, std::cmp::PartialOrd, std::cmp::Eq, std::cmp::Ord)]
pub struct ProjectCategory {
    pub id: i32,
    pub name: String,
    pub picture_url: Option<String>,
    pub slug: String,
    pub deleted_at: Option<NaiveDateTime>,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

impl ProjectCategory {
    fn pretty_print(&self) {
        println!("id: {}, title: {}", &self.id, &self.name);
    }
}

fn build_url(path: &str) -> Url{
    let url = String::from( "http://localhost:8088");

    let url = format!("{}{}", url, path);
    let message = "Invalid url :";
    let message = format!("{} {}", message, &url);
    Url::parse(&url).expect(&message)
}

struct Repository {}

impl Repository {

    async fn get_project(&self, project_id: i32) -> Result<Project, reqwest::Error> {
        let base_url = String::from("/projects/");
        let base_url = format!("{}{}", base_url, project_id.to_string());
        let url: Url = build_url(&base_url);
        let client = reqwest::Client::new();
        let project = client.get(url)
            .header("Authorization", request_auth())
            .send()
            .await?
            .json::<Project>()
            .await?;

        Ok(project)
    }

    async fn get_projects(&self) -> Result<Vec<Project>, reqwest::Error> {

        let url: Url = build_url("/projects");
        let client = reqwest::Client::new();
        let mut projects = client.get(url)
            .header("Authorization", request_auth())
            .send()
            .await?
            .json::<Vec<Project>>()
            .await?;

        projects.sort();

        Ok(projects)
    }

    fn selectify_projects(&self, projects: &Vec<Project>) -> Vec<String> {
        projects.into_iter().map(
            | x | format!(
                "id: {}, title: {}"
                , x.id.to_string()
                , x.title
            )
        ).collect()
    }

    async fn print_projects (&self) -> Result<(), reqwest::Error>  {
        let projects = &self.get_projects().await.unwrap();
        for project in projects {
            project.pretty_print();
        }
        Ok(())
    }

    async fn get_categories(&self) -> Result<Vec<ProjectCategory>, reqwest::Error> {
        let url: Url = build_url("/categories");
        let client = reqwest::Client::new();
        let mut categories = client.get(url)
            .header("Authorization", request_auth())
            .send()
            .await?
            .json::<Vec<ProjectCategory>>()
            .await?;

        categories.sort();
        Ok(categories)
    }

    async fn print_categories (&self) -> Result<(), reqwest::Error> {
        let categories = &self.get_categories().await.unwrap();
        for category in categories {
            category.pretty_print();
        }
        Ok(())
    }

    fn selectify_categories(&self, categories: &Vec<ProjectCategory>) -> Vec<String> {
        categories.into_iter().map(| x | format!("id: {}, name: {}", x.id, x.name)).collect()
    }

    async fn get_image_categories(&self) -> Result<Vec<ImageCategory>, reqwest::Error> {
        let url: Url = build_url("/projectImageCategories");
        let client = reqwest::Client::new();
        let mut categories = client.get(url)
            .header("Authorization", request_auth())
            .send()
            .await?
            .json::<Vec<ImageCategory>>()
            .await?;

        categories.sort();
        Ok(categories)
    }

    fn selectify_image_categories(&self, categories: &Vec<ImageCategory>) -> Vec<String> {
        categories.into_iter().map(| x | format!("id: {}, name: {}", x.id, x.name)).collect()
    }

    async fn print_image_categories (&self) -> Result<(), reqwest::Error> {
        let categories = &self.get_image_categories().await.unwrap();
        for category in categories {
            category.pretty_print();
        }
        Ok(())
    }
}


#[derive(serde::Deserialize,serde::Serialize, Debug)]
struct ImageForm {
    project_image_category: u32,
    primary: bool
}

impl ImageForm {
    fn upload(&self, image: Vec<u8>, file_name: String, project_id: &i32, image_mime: mime::Mime) -> Result<Image, String> {
         let path = format!(
             "/projectImage?project_id={}&category_id={}&primary={}",
             project_id.to_string(),
             self.project_image_category.to_string(),
             self.primary
         );
        let url: Url = build_url(&path);
        let part = blocking::multipart::Part::bytes(image).file_name(file_name).mime_str(image_mime.essence_str()).expect("Failed building part");
        let form = blocking::multipart::Form::new().part("file", part);
        let client = blocking::Client::new();
        let resp = client.post(url)
            .header("Authorization", request_auth())
            .timeout(Duration::from_secs(240))
            .multipart(form)
            .send()
            .unwrap();

         return match  resp.status() {
             StatusCode::OK => {
                 let image = resp.json::<Image>().unwrap();
                 Ok(image)
             }
             _ => {
                 let text = resp.text().unwrap();
                 Err(text)
             }
         }
    }
     fn open_and_upload (&self, file_path: String, project_id: &i32) -> Result<Image, String> {
         let path = format!(
             "/projectImage?project_id={}&category_id={}&primary={}",
             project_id.to_string(),
             self.project_image_category.to_string(),
             self.primary
         );
        let url: Url = build_url(&path);
        println!("{}", file_path);
        // let reader = File::open(file_path).unwrap();
        // let part = blocking::multipart::Part::bytes(reader).file_name("foobar");
        let form = blocking::multipart::Form::new().file("file", file_path).expect("Could not read file");
        let client = blocking::Client::new();
        let resp = client.post(url)
            .header("Authorization", request_auth())
            .timeout(Duration::from_secs(240))
            .multipart(form)
            .send()
            .unwrap();

         return match  resp.status() {
             StatusCode::OK => {
                 let image = resp.json::<Image>().unwrap();
                 Ok(image)
             }
             _ => {
                 let text = resp.text().unwrap();
                 Err(text)
             }
         }
     }
}

#[derive(serde::Deserialize,serde::Serialize, Debug)]
struct Image {
    id: u32,
    w1500_keyname: String,
    w350_keyname: String,
    project_image_category_id: u32,
    project_id: u32,
    w1500_object_url: String,
    w350_object_url: String,
    primary: bool,
}

impl Image {
    fn pretty_print(&self) {
        println!("id: {} project_id: {}, category_id: {}, primary: {}", &self.id, &self.project_id, &self.project_image_category_id, &self.primary)
    }
}

#[derive(serde::Deserialize,serde::Serialize, Debug, Eq, Ord, PartialEq, PartialOrd)]
struct ImageCategory {
    id: u32,
    name: String
}

impl ImageCategory {
    fn pretty_print(&self) {
        println!("id: {}, name: {}", &self.id, &self.name)
    }
}

fn gen_tmp_filename() -> std::path::PathBuf {
    let name = gen_random_string();
    let mut path = temp_dir();
    path.push(format!("{}.md", name));
    path
}

fn gen_random_string () -> String  {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(10)
        .collect::<String>()
}

fn output<T: std::fmt::Display>(res: Result<(), T>)-> Result<(), String> {
    match res {
        Ok(value) => Ok(value),
        Err(err) => Err( err.to_string() )
    }
}

#[tokio::main]
async fn main() -> Result<(), String> {
    let args = Ctprods::from_args();
    let repo = Repository{};
    match args {
        Ctprods::List => {
            let entities = vec![Entity::projects, Entity::categories, Entity::image_categories];
            let selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Que voulez vous lister ?")
                .default(0)
                .items(&entities[..])
                .interact()
                .unwrap();
            let entity: &Entity = entities.get(selection).unwrap();
            let res: Result<(), reqwest::Error> = match entity {
                Entity::projects => repo.print_projects().await,
                Entity::categories => repo.print_categories().await,
                Entity::image_categories => repo.print_image_categories().await
            };
            output(res)
        },
        Ctprods::Create => {
            let categories: Vec<ProjectCategory> = repo.get_categories().await.unwrap();
            let selectified_categories: Vec<String> = repo.selectify_categories(&categories);
            loop {
                let title = Input::<String>::new().with_prompt("Titre").interact();
                let title: String = match title {
                    Ok(title) => title,
                    Err(_) => continue
                };
                loop {
                    let selection = Select::with_theme(&ColorfulTheme::default())
                        .with_prompt("Choisir une catégorie")
                        .default(0)
                        .items(&selectified_categories[..])
                        .interact()
                        .unwrap();
                    let selected_category: &ProjectCategory = categories.get(selection).unwrap();
                    let is_pro = Confirm::new().with_prompt("Le projet est il un projet professionnel ?").interact().unwrap();
                    let has_bitbucket_project_key = Confirm::new().with_prompt("Le projet a-t'il une clé de projet bitbucket ?").interact().unwrap();
                    let bitbucket_project_key = match has_bitbucket_project_key {
                        true =>
                            Input::<String>::new().with_prompt("Quelle est la clé du projet Bitbucket ?").interact().ok(),
                        false =>
                            Option::None
                    };
                    let mut p = NewProject {
                        title,
                        content: String::from(""),
                        category_id: selected_category.id,
                        user_id: 1,
                        sketchfab_model_number: None,
                        slug: None,
                        is_pro: is_pro,
                        bitbucket_project_key: bitbucket_project_key,
                    };
                    let p = p.edit();
                    let out: Result<Project, String> = p.save().await;
                    match out {
                        Ok(project) => {
                            println!("{}", "Success !");
                            project.pretty_print()
                        },
                        Err(err) => println!("{}", err)
                    }
                    break;
                }
                break;
            }
            Ok(())
        },
        Ctprods::Edit => {
            let projects: Vec<Project> = repo.get_projects().await.unwrap();
            let selectified_projects: Vec<String> = repo.selectify_projects(&projects);
            let selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Choisir un projet")
                .default(0)
                .paged(true)
                .items(&selectified_projects[..])
                .interact()
                .unwrap();
            let selected_project: &Project = projects.get(selection).unwrap();
            let project_id = selected_project.id;
            let mut project: Project = repo.get_project(project_id).await.unwrap();
            let project_to_save = &project.edit();
            let out = project_to_save.update().await;
            match out {
                Ok(project_algolia) => {
                    println!("{}", "Success !");
                    project_algolia.pretty_print()
                },
                Err(err) => println!("{}", err)
            }
            Ok(())
        },
        Ctprods::AddImage {file} => {
            let projects: Vec<Project> = repo.get_projects().await.unwrap();
            let selectified_projects: Vec<String> = repo.selectify_projects(&projects);
            let categories: Vec<ImageCategory> = repo.get_image_categories().await.unwrap();
            let selectified_categories: Vec<String> = repo.selectify_image_categories(&categories);
            let selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Choisir un projet")
                .default(0)
                .paged(true)
                .items(&selectified_projects[..])
                .interact()
                .unwrap();
            let selected_project: &Project = projects.get(selection).unwrap();

            let image_category_selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Choisir une catégorie")
                .default(0)
                .paged(true)
                .items(&selectified_categories[..])
                .interact()
                .unwrap();
            let selected_category = categories.get(image_category_selection).unwrap();
            let id = selected_project.id;
            let is_cover_image: bool = Confirm::new().with_prompt("Is it the cover image?").interact().unwrap();
            let i = ImageForm {
                project_image_category: selected_category.id,
                primary: is_cover_image
            };
            let parsed = Url::parse(&file);
            match parsed {
                Ok(url) => {
                    let client = reqwest::Client::new();
                    let path = url.path();
                    let split_path = path.clone().split("/");
                    let split_ext = path.clone().split(".");
                    let ext =       &split_ext.collect::<Vec<&str>>().pop().expect("Could not get file extension from url");
                    let file_name: String = split_path.collect::<Vec<&str>>().pop().expect("Cannot read file name from url Path").to_string();
                    let mime = MimeGuess::from_ext(ext);
                    let resp = client.get(&url.to_string());
                    if let Ok(res) = resp.send().await {
                        let image: Vec<u8> = res.bytes().await.expect("Invalid Image downloaded").to_vec();
                        let out = task::spawn_blocking(move || i.upload(image, file_name, &id, mime.first_or(mime::IMAGE_JPEG))).await.unwrap();
                        match out {
                            Ok(img) => {
                                println!("{}", "Success !");
                                img.pretty_print()
                            },
                            Err(err) => println!("{}", err)
                        }
                    } else {
                        println!("Cannot GET image");
                    }
                },
                _ => {
                    let out = task::spawn_blocking(move || i.open_and_upload(String::from(file), &id)).await.unwrap();
                    match out {
                        Ok(img) => {
                            println!("{}", "Success !");
                            img.pretty_print()
                        },
                        Err(err) => println!("{}", err)
                    }
                }
            }
            Ok(())
        },
        Ctprods::Publish => {
            let projects: Vec<Project> = repo.get_projects().await.unwrap();
            let selectified_projects: Vec<String> = repo.selectify_projects(&projects);
            let selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Choisir un projet")
                .default(0)
                .paged(true)
                .items(&selectified_projects[..])
                .interact()
                .unwrap();
            let selected_project: &Project = projects.get(selection).unwrap();
            let result = selected_project.publish().await;
            match result {
                Ok(project) => println!("Successfully published project {}", project.title),
                Err(err) => println!("Error while publishing: {}", err)
            }
            Ok(())
        },

        Ctprods::Unpublish => {
            let projects: Vec<Project> = repo.get_projects().await.unwrap();
            let selectified_projects: Vec<String> = repo.selectify_projects(&projects);
            let selection = Select::with_theme(&ColorfulTheme::default())
                .with_prompt("Choisir un projet")
                .default(0)
                .paged(true)
                .items(&selectified_projects[..])
                .interact()
                .unwrap();
            let selected_project: &Project = projects.get(selection).unwrap();
            let result = selected_project.unpublish().await;
            match result {
                Ok(project) => println!("Successfully unpublished project {}", project.title),
                Err(err) => println!("Error while publishing: {}", err)
            }
            Ok(())
        }
    }
}
